A cpu process scheduling simulator including priority and round robin algorithms.
Usage: java Scheduler <filename> <print interval> <time quantum>

filename: Name of file containing data of the processes to be executed.
print interval: Time units in between each printing of the cpu's current state.
time quantum: Time a process will get to spend being executed before being context switched in the Round Robin algorithm.

Each line of the file represents a process. Format of each line should be as follows:
<pid> <CPUBurst> <I/OBurst> <Priority> 

If there is a IOBurst it will take place halfway through the processes CPUBurst.
A sample file named "testData.txt" has been provided.

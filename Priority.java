/*MacKinley Trudeau*/

import java.io.*;
import java.util.*;

public class Priority
{
	//CPU queue
	private ProcessQueue cpuQueue;
	//IO queue
	private ProcessQueue ioQueue;
	//queue of completed processes
	private ProcessQueue completedQueue;

	//keeps track of last process in cpu to use in cpu sequence
	private Process lastInCpu;

	//keeps track of time passed
	private int totalTime;
	//keeps track of context switches
	private int numContextSwitches;
	//total number of processes
	private int totalNumProcesses;
	//hold total wait time of all processes
	private int totalWaitTime;
	//holds total turnaround time of all processes
	private int totalTurnaround;
	//time cpu was inactive
	private int cpuInactive;
	//tell whether cpu is empty or not
	private boolean thereWasContextSwitch;
	//contains the pids of processes that entered the cpu
	private String cpuSequence, whatFinished, whatFinishedIO;

	public Priority()
	{
		cpuQueue = new ProcessQueue();
		ioQueue = new ProcessQueue();
		completedQueue = new ProcessQueue();

		lastInCpu = null;

		totalTime = 0;
		numContextSwitches = 0;
		totalNumProcesses = 0;
		totalWaitTime = 0;
		totalTurnaround = 0;
		cpuInactive = 0;
		thereWasContextSwitch = true;
		cpuSequence = "";
		whatFinished = "";
		whatFinishedIO = "";
	}

	public Priority(String fileName)
	{
		cpuQueue = new ProcessQueue();
		ioQueue = new ProcessQueue();
		completedQueue = new ProcessQueue();

		lastInCpu = null;

		totalTime = 0;
		numContextSwitches = 0;
		totalNumProcesses = 0;
		totalWaitTime = 0;
		totalTurnaround = 0;
		cpuInactive = 0;
		thereWasContextSwitch = true;
		cpuSequence = "";
		whatFinished = "";
		whatFinishedIO = "";

		//Open file
		try
		{
    		//Scan through file line by line
    		File input = new File(fileName);
     		BufferedReader lReader = new BufferedReader(new FileReader(input));
     		String line, word;
     		Process temp;
           
        	//Read line by line and create and load all processes into queue in order of arrival
    		while((line = lReader.readLine()) != null)
    		{
    			//reads each "word" on the current line
    			StringTokenizer wReader = new StringTokenizer(line, " ");

    			//create a new process but don't add it to the queue yet
    			temp = new Process();

    			/*get its attributes and set them to the process*/

    			//pid
    			word = wReader.nextToken().trim();
    			temp.setPid(Integer.parseInt(word));
    			//cpu Burst
    			word = wReader.nextToken().trim();
    			temp.setCpuBurst(Integer.parseInt(word));
    			//I/O Burst
    			word = wReader.nextToken().trim();
    			temp.setIoBurst(Integer.parseInt(word));
    			//if there is a io burst time
    			if(temp.getIoBurst() != 0)
    				temp.setIoBurstStartTime(temp.getCpuBurst()/2);
    			else//set it to 0 if there isn't a io burst time so it never happens
    				temp.setIoBurstStartTime(0);
    			//priority
    			word = wReader.nextToken().trim();
    			temp.setPriority(Integer.parseInt(word));

    			cpuQueue.insert(temp, 'p');
    		}
    	}catch(FileNotFoundException e)
	    {
	    	System.out.println("Error opening");
	    }catch(IOException e)
	    {
	      	System.out.println("Error reading file");
	    }catch(Exception e)
	    {
	      	System.out.println("Error opening or reading file: " + e.getMessage());
	    }

	    totalNumProcesses = cpuQueue.getSize();
	}

	public void execute()
	{
		//references to front of both queues
		Process cFront, ioFront, cTemp, ioTemp;
		//will hold all the processes coming out of io so they can be inserted by pid
		ProcessQueue tempQ = new ProcessQueue();

		cFront = cpuQueue.getFront();
		ioFront = ioQueue.getFront();

		//reset what finished for new moment of execution
		whatFinished = "";
		whatFinishedIO = "";

		/*								*/
		/*								*/
		/*execute process and service io*/
		/*								*/
		/*								*/

		//if something is in the cpu queue
		//this will decrement burst time and increment wait times
		//context switching will happen after io devices are serviced
		if(cFront != null)
		{
			//reduce cpu time of front process by 1
			cFront.setCpuBurst(cFront.getCpuBurst() - 1);

			//get first waiting process
			cTemp = cFront.getNext();

			if(thereWasContextSwitch)
			{
				if(cpuSequence != "")
					cpuSequence = cpuSequence + "-" + Integer.toString(cFront.getPid());
				else
					cpuSequence = Integer.toString(cFront.getPid());

				numContextSwitches++;
			}

			//cpu is serving a process so its not empty
			thereWasContextSwitch = false;

			//increment wait time for all other processes in cpu queue
			while(cTemp != null)
			{
				cTemp.setWaitTime(cTemp.getWaitTime() + 1);
				//get next process
				cTemp = cTemp.getNext();
			}
		}
		else
			cpuInactive++;

		//assign last in cpu to what just got procesed
		lastInCpu = cFront;

		//get first io queue process
		ioTemp = ioFront;

		//if something is in the io queue
		while(ioTemp != null)
		{
			//reduce io time of all process by 1
			ioTemp.setIoBurst(ioTemp.getIoBurst() - 1);
			ioTemp = ioTemp.getNext();
		}

		totalTime++;

		/*							 */
		/*							 */
		/*check for context switching*/
		/*							 */
		/*							 */

		/*CPU context switches*/

		//if something is in cpuQueue
		if(cFront != null)
		{
			//check if front process is done executing
			if(cFront.getCpuBurst() <= 0)
			{
				//save that this job finished executing
				whatFinished = "Job " + Integer.toString(cFront.getPid()) + " DONE\n";

				cFront.setTurnAroundTime(totalTime);
				completedQueue.insert(cpuQueue.dequeue(), 'c');

				//procces must be contexted switch out/in
				thereWasContextSwitch = true; 
			}else
			//check if its halfway completed and has a ioburst
			if(cFront.getCpuBurst() == cFront.getIoBurstStartTime())
			{
				//save that this job finished its cpu burst
				whatFinished = "Job " + Integer.toString(cFront.getPid()) + " finished CPU burst\n";

				cFront.setIoBurstStartTime(0);

				//remove front from the cpuqueue and add it to the ioqueue
				ioQueue.enqueue(cpuQueue.dequeue());

				//procces must be contexted switch out/in
				thereWasContextSwitch = true; 
			}
		}

		/*io context switching*/

		if(!ioQueue.isEmpty())
		{
			int y = ioQueue.getSize();
			//while(ioFront != null)
			for(int x = 0; x < y; x++)
			{
				ioFront = ioQueue.getFront();

				//check if front process is done executing
				if(ioFront.getIoBurst() <= 0)
				{	
					if(whatFinishedIO == "")
						whatFinishedIO = Integer.toString(ioFront.getPid());
					else
						whatFinishedIO = whatFinishedIO + " & " + Integer.toString(ioFront.getPid());

					//if so put it in the temp queue
					cpuQueue.insert(ioQueue.dequeue(), 'p');
					//check if it was switched to front of cpuQueue
					if(ioFront == cpuQueue.getFront())
					{
						thereWasContextSwitch = true;
						//since ioFront is now the front of the cpuQueue
						if(ioFront.getNext() != null)
							whatFinished = "Job " + Integer.toString(ioFront.getNext().getPid()) + 
							" was context switched out because of job " + Integer.toString(ioFront.getPid()) + "\n";
					}
				}
				else
				{	//cycle front to back to get to the next processes
					ioQueue.enqueue(ioQueue.dequeue());
				}
			}
		}
		
	}

	public ProcessQueue getCpuQueue()
	{
		return cpuQueue;
	}

	public ProcessQueue getIoQueue()
	{
		return ioQueue;
	}

	public ProcessQueue getCompletedQueue()
	{
		return completedQueue;
	}

	public int getTotalTime()
	{
		return totalTime;
	}

	public String getCpuSequence()
	{
		return cpuSequence;
	}

	public float getThroughput()
	{
		return totalNumProcesses/(float)totalTime;
	}

	public float getCpuUtil()
	{
		return (totalTime - cpuInactive)/(float)totalTime;
	}

	public float getAvgWaitingTime()
	{
		return totalWaitTime/(float)totalNumProcesses;
	}

	public float getAvgTurnAroundTime()
	{
		return totalTurnaround/(float)totalNumProcesses;
	}

	public int getNumContextSwitches()
	{
		return numContextSwitches;
	}

	public void printCurrentState()
	{	
		Process temp = cpuQueue.getFront();
		//print current time
		System.out.println("\nt = " + totalTime);
		//print what finished
		System.out.print(whatFinished);

		//print state of cpu
		//if nothing is in cpu
		if(thereWasContextSwitch)
		{
			//and there is nothing ready
			if(cpuQueue.isEmpty())
				System.out.println("CPU waiting");
			else
				System.out.println("CPU loading job " + Integer.toString(temp.getPid()) +
					": CPU burst(" + (temp.getCpuBurst() - temp.getIoBurstStartTime()) + ") IO burst(" + temp.getIoBurst() + ")");
		}
		else
		{
			System.out.println("Servicing PS job " + Integer.toString(temp.getPid()) +
					": CPU burst(" + (temp.getCpuBurst() - temp.getIoBurstStartTime()) + ") IO burst(" + temp.getIoBurst() + ")");
		}


		/*print ready queue*/

		System.out.print("Current state of ready queue: ");
		//if first process is in the cpu skip it
		if(!thereWasContextSwitch)
			temp = temp.getNext();

		//print first ready
		if(temp != null)
		{
			System.out.print(Integer.toString(temp.getPid()));
			temp = temp.getNext();

			//print rest ready
			while(temp != null)
			{
				System.out.print("-" + Integer.toString(temp.getPid()));
				temp = temp.getNext();
			}
		}
		else
			System.out.print("EMPTY");
		System.out.println();
		/*print IO queue*/

		//print what finished IO
		if(whatFinishedIO != "")
			System.out.println("Job(s) " + whatFinishedIO + " finished IO burst");

		//print current state of IO queue

		temp = ioQueue.getFront();

		System.out.print("Current state of IO queue: ");

		//print first (for formatting)
		if(temp != null)
		{
			System.out.print(Integer.toString(temp.getPid()));
			temp = temp.getNext();

			//then print the rest (for formatting)
			while(temp != null)
			{
				System.out.print("-" + Integer.toString(temp.getPid()));
				temp = temp.getNext();
			}
		}
		else
			System.out.println("EMPTY");

		System.out.println();
	}

	public void printReport()
	{
		System.out.print("\nFinal report for PS algorithm.\n\nThroughput: ");

		System.out.println(this.getThroughput() + "\n\nProcess ID\t Wait Time");
		for(int x = 0; x < totalNumProcesses; x++)
		{
			//print pid and wait time
			System.out.println("    " + completedQueue.getFront().getPid() + "\t\t    " + completedQueue.getFront().getWaitTime());
			//add all wait times while we are at it
			totalWaitTime += completedQueue.getFront().getWaitTime();
			completedQueue.enqueue(completedQueue.dequeue());
		}
		System.out.println("Average waiting time = " + this.getAvgWaitingTime());

		System.out.println("\nCPU utilization: " + (int)(this.getCpuUtil()*100) + "%");

		System.out.println("\nSequence of process in CPU: " + cpuSequence);


		System.out.println("\nProcess ID\t Turnaround Time");
		for(int x = 0; x < totalNumProcesses; x++)
		{
			//print pid and wait time
			System.out.println("    " + completedQueue.getFront().getPid() + "\t\t\t" + completedQueue.getFront().getTurnAroundTime());
			//add all wait times while we are at it
			totalTurnaround += completedQueue.getFront().getTurnAroundTime();
			completedQueue.enqueue(completedQueue.dequeue());
		}
		System.out.println("Average turnaround time = " + this.getAvgTurnAroundTime());

		System.out.println("\nNumber of context switches " + this.getNumContextSwitches());
	}
}
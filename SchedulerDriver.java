/*MacKinley Trudeau*/

import java.io.*;

public class SchedulerDriver
{
	public static void main(String args[])
	{
		int printInterval, timeQ;

		String fileName, usage = "\nUsage: java Scheduler <string: file name> <int: print interval> <int: time quantum>\n";
		
		if(args.length == 1 && args[0].compareToIgnoreCase("help") == 0)
		{
			System.out.println(usage + "\n" + "File Name: Name of file containing data of the processes to be executed.\n\nPrint Interval: Time units in between each printing of the cpu's current state.\n\nTime Quantum: Time a process will get to spend being executed before being context switched in the Round Robin algorithm.\n");
			System.out.println("Each line of the file represents a process.\nFormat of the file is as follows:\n<pid> <CPUBurst> <I/OBurst> <Priority> (Only integers and spaces)\n\nIf there is a IOBurst it will take place halfway through the processes CPUBurst.\n\nA sample file named \"data.txt\" has been provided.\n");
			return;
		}
		else
		if(args.length != 3)
		{
			System.out.println(usage + "\n" + "Enter \"java SchedulerDriver help\" for more detail.\n");
			return;
		}

		try
		{
			//get arguments
			fileName = args[0];
			printInterval = Integer.parseInt(args[1]);
			timeQ = Integer.parseInt(args[2]);
		}catch(NumberFormatException e)
		{
			System.out.println("\n" + e.toString() + "\n" + usage);
			return;
		}catch(Exception e)
		{
			System.out.println("\n" + e.toString() + "\n" + usage);
			return;
		}

		Priority pr = new Priority(fileName);

		System.out.println("********************** Priority Scheduling **********************");

		//while there is at least 1 process in one of the queues
		while(!pr.getCpuQueue().isEmpty() || !pr.getIoQueue().isEmpty())
		{
			//print current state at given interval
			if(pr.getTotalTime() % printInterval == 0)
				pr.printCurrentState();

			//execute by 1 time unit
			pr.execute();
		}

		System.out.println();

		//print final report
		pr.printReport();


		RoundRobin rr = new RoundRobin(fileName, timeQ);

		System.out.println("\n\n\n********************** Round Robin **********************");

		//while there is at least 1 process in one of the queues
		while(!rr.getCpuQueue().isEmpty() || !rr.getIoQueue().isEmpty())
		{
			//print current state at given interval
			if(rr.getTotalTime() % printInterval == 0)
				rr.printCurrentState();

			//execute by 1 time unit
			rr.execute();
		}

		System.out.println();

		//print final report
		rr.printReport();

	}
}
/*MacKinley Trudeau*/

public class ProcessQueue
{
	//reference to process in front of the queue
	private Process front;

	//reference to process in back of the queue
	private Process back;

	//hold number of proccess in queue
	int numProcesses;

	//returns size(number of custs in queue)
	public int getSize()
	{
		return numProcesses;
	}
	  
	//sets size
	public void setSize(int n)
	{
		numProcesses = n;
	}

	//returns true if queue is empty and false otherwise
	public boolean isEmpty()
	{
		return (numProcesses == 0);
	}

	//returns the front of the queue
	public Process getFront()
	{
		return front;
	}

	//sets the front of the queue
	public void setFront(Process p)
	{
		front = p;
	}

	//returns the back of the queue
	public Process getBack()
	{
		return back;
	}

	//sets the back of the queue
	public void setBack(Process p)
	{
		back = p;
	}

	//adds process to back of queue and increments numProcesses
	public void enqueue(Process p)
	{
		if(this.isEmpty())
		{
			front = p;
			back = p;
		}
		else
		{
			back.setNext(p);
			back = p;
		}
		numProcesses++;
	}

	//removes process from front of queue
	//set the process behind them to the front of queue
	//and decrements numProcesses
	public Process dequeue()
	{
		Process temp = front;

		if(numProcesses == 0)
			return null;
		else
		if(numProcesses == 1)
		{
			front = null;
		}
		else
		{
			front = front.getNext();
			temp.setNext(null);
		}

		numProcesses--;

		return temp;
	}

	//Insert a process into queue given a flag to determine which algorithm to use
	//flag = p for prioity
	//flag = s for shortest job first
	public void insert(Process p, char flag)
	{
		//starting reference and 
		Process temp = front, temp2 = null;

		if(this.isEmpty())
		{
			this.enqueue(p);
			return;
		}
		//for prioity scheduling
		if(flag == 'p')
		{
			//find posistion to insert process p
			while(temp != null && p.getPriority() > temp.getPriority())
			{
				temp2 = temp;
				temp = temp.getNext();
			}


			while(temp != null && p.getPriority() == temp.getPriority() && p.getPid() > temp.getPid())
			{
				temp2 = temp;
				temp = temp.getNext();
			}

			/*insert process p*/

			//if it belongs in the front
			if(temp == front)
			{
				p.setNext(temp);
				front = p;
			}
			//if it belongs at the end
			if(temp == null)
			{
				this.enqueue(p);
				return;
			}
			else
			{
				p.setNext(temp);
				if(temp2 != null)
					temp2.setNext(p);
				else
					front = p;
			}
		}
		//for shortest job remaining
		else if(flag == 's')
		{

			int index = 0;
			//find posistion to insert process p
			while(temp != null && p.getCpuBurst() > temp.getCpuBurst())
			{
				temp = temp.getNext();
				index++;
			}

			//find were to insert amongst equal cpu burst
			while(temp != null && p.getCpuBurst() == temp.getCpuBurst() && p.getPid() > temp.getPid())
			{
				temp = temp.getNext();
				index++;
			}

			//insert process p
			//cpuQueue.add(index, p);

		}
		else if(flag == 'c')
		{
			/*inserts by pid for copmleted queue*/

			//find position to insert process p
			while(temp != null && p.getPid() > temp.getPid())
			{
				temp2 = temp;
				temp = temp.getNext();
			}
			//if it belongs in the front
			if(temp == front)
			{
				p.setNext(temp);
				front = p;
			}
			//if it belongs at the end
			if(temp == null)
			{
				this.enqueue(p);
				return;
			}
			else
			{
				p.setNext(temp);
				if(temp2 != null)
					temp2.setNext(p);
				else
					front = p;
			}
		}else
		{
			System.out.println("Insert error. Flag not recognized. \nPlease use p for priority queue and s for shortest job remaining.");
		}

		numProcesses++;
	}

	//clears queue
	public void clear()
	{
		while(!isEmpty())
		{
			dequeue();
		}
	}
}
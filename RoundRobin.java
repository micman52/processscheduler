/*MacKinley Trudeau*/

import java.io.*;
import java.util.*;

public class RoundRobin
{
	//CPU queue
	private ProcessQueue cpuQueue;
	//IO queue
	private ProcessQueue ioQueue;
	//queue of completed processes
	private ProcessQueue completedQueue;

	//keeps track of last process in cpu to use in cpu sequence
	private Process lastInCpu;

	//keeps track of time passed
	private int totalTime;
	//keeps track of context switches
	private int numContextSwitches;
	//total number of processes
	private int totalNumProcesses;
	//hold total wait time of all processes
	private int totalWaitTime;
	//holds total turnaround time of all processes
	private int totalTurnaround;
	//time cpu was inactive
	private int cpuInactive;
	//tell whether cpu is empty or not
	private boolean cpuIsEmpty;
	//contains the pids of processes that entered the cpu
	private String cpuSequence, whatFinished, whatFinishedIO;

	//time quantum constant
	private int timeQ;
	//keeps track of time for time quantum purposes
	private int timePerBurst;

	public RoundRobin()
	{
		cpuQueue = new ProcessQueue();
		ioQueue = new ProcessQueue();
		completedQueue = new ProcessQueue();

		lastInCpu = null;

		timeQ = 3;
		timePerBurst = 0;

		totalTime = 0;
		numContextSwitches = 0;
		totalNumProcesses = 0;
		totalWaitTime = 0;
		totalTurnaround = 0;
		cpuInactive = 0;
		cpuIsEmpty = true;
		cpuSequence = "";
		whatFinished = "";
		whatFinishedIO = "";
	}

	public RoundRobin(String fileName)
	{
		cpuQueue = new ProcessQueue();
		ioQueue = new ProcessQueue();
		completedQueue = new ProcessQueue();

		lastInCpu = null;

		timeQ = 3;
		timePerBurst = 0;

		totalTime = 0;
		numContextSwitches = 0;
		totalNumProcesses = 0;
		totalWaitTime = 0;
		totalTurnaround = 0;
		cpuInactive = 0;
		cpuIsEmpty = true;
		cpuSequence = "";
		whatFinished = "";
		whatFinishedIO = "";

		//Open file
		try
		{
    		//Scan through file line by line
    		File input = new File(fileName);
     		BufferedReader lReader = new BufferedReader(new FileReader(input));
     		String line, word;
     		Process temp;
           
        	//Read line by line and create and load all processes into queue in order of arrival
    		while((line = lReader.readLine()) != null)
    		{
    			//reads each "word" on the current line
    			StringTokenizer wReader = new StringTokenizer(line, " ");

    			//create a new process but don't add it to the queue yet
    			temp = new Process();

    			/*get its attributes and set them to the process*/

    			//pid
    			word = wReader.nextToken().trim();
    			temp.setPid(Integer.parseInt(word));
    			//cpu Burst
    			word = wReader.nextToken().trim();
    			temp.setCpuBurst(Integer.parseInt(word));
    			//I/O Burst
    			word = wReader.nextToken().trim();
    			temp.setIoBurst(Integer.parseInt(word));
    			//if there is a io burst time
    			if(temp.getIoBurst() != 0)
    				temp.setIoBurstStartTime(temp.getCpuBurst()/2);
    			else//set it to 0 if there isn't a io burst time so it never happens
    				temp.setIoBurstStartTime(0);
    			//priority
    			word = wReader.nextToken().trim();
    			temp.setPriority(Integer.parseInt(word));

    			cpuQueue.enqueue(temp);
    		}
    	}catch(FileNotFoundException e)
	    {
	    	System.out.println("Error opening");
	    }catch(IOException e)
	    {
	      	System.out.println("Error reading file");
	    }catch(Exception e)
	    {
	      	System.out.println("Error opening or reading file: " + e.getMessage());
	    }
	    totalNumProcesses = cpuQueue.getSize();
	}

	public RoundRobin(String fileName, int timeQ)
	{
		cpuQueue = new ProcessQueue();
		ioQueue = new ProcessQueue();
		completedQueue = new ProcessQueue();

		lastInCpu = null;

		this.timeQ = timeQ;
		timePerBurst = 0;

		totalTime = 0;
		numContextSwitches = 0;
		totalNumProcesses = 0;
		totalWaitTime = 0;
		totalTurnaround = 0;
		cpuInactive = 0;
		cpuIsEmpty = true;
		cpuSequence = "";
		whatFinished = "";
		whatFinishedIO = "";

		//Open file
		try
		{
    		//Scan through file line by line
    		File input = new File(fileName);
     		BufferedReader lReader = new BufferedReader(new FileReader(input));
     		String line, word;
     		Process temp;
           
        	//Read line by line and create and load all processes into queue in order of arrival
    		while((line = lReader.readLine()) != null)
    		{
    			//reads each "word" on the current line
    			StringTokenizer wReader = new StringTokenizer(line, " ");

    			//create a new process but don't add it to the queue yet
    			temp = new Process();

    			/*get its attributes and set them to the process*/

    			//pid
    			word = wReader.nextToken().trim();
    			temp.setPid(Integer.parseInt(word));
    			//cpu Burst
    			word = wReader.nextToken().trim();
    			temp.setCpuBurst(Integer.parseInt(word));
    			//I/O Burst
    			word = wReader.nextToken().trim();
    			temp.setIoBurst(Integer.parseInt(word));
    			//if there is a io burst time
    			if(temp.getIoBurst() != 0)
    				temp.setIoBurstStartTime(temp.getCpuBurst()/2);
    			else//set it to 0 if there isn't a io burst time so it never happens
    				temp.setIoBurstStartTime(0);
    			//priority
    			word = wReader.nextToken().trim();
    			temp.setPriority(Integer.parseInt(word));

    			cpuQueue.enqueue(temp);
    		}
    	}catch(FileNotFoundException e)
	    {
	    	System.out.println("Error opening");
	    }catch(IOException e)
	    {
	      	System.out.println("Error reading file");
	    }catch(Exception e)
	    {
	      	System.out.println("Error opening or reading file: " + e.getMessage());
	    }
	    totalNumProcesses = cpuQueue.getSize();
	}

	public void execute()
	{
		//references to front of both queues
		Process cFront, ioFront, cTemp, ioTemp;
		//will hold all the processes coming out of io so they can be inserted by pid
		ProcessQueue tempQ = new ProcessQueue();

		//reset what finished for new moment of execution
		whatFinished = "";
		whatFinishedIO = "";

		cFront = cpuQueue.getFront();
		ioFront = ioQueue.getFront();

		/*								*/
		/*								*/
		/*execute process and service io*/
		/*								*/
		/*								*/

		//if something is in the cpu queue
		//this will decrement burst time and increment wait times
		//context switching will happen after io devices are serviced
		if(cFront != null)
		{
			//reduce cpu time of front process by 1
			cFront.setCpuBurst(cFront.getCpuBurst() - 1);

			//get first waiting process
			cTemp = cFront.getNext();
			if(cpuIsEmpty)
			{
				if(cpuSequence != "")
					cpuSequence = cpuSequence + "-" + Integer.toString(cFront.getPid());
				else
					cpuSequence = Integer.toString(cFront.getPid());
			}

			//cpu is serving a process so its not empty
			cpuIsEmpty = false;

			//increment wait time for all other processes in cpu queue
			while(cTemp != null)
			{
				cTemp.setWaitTime(cTemp.getWaitTime() + 1);
				//get next process
				cTemp = cTemp.getNext();
			}

			timePerBurst++;
		}
		else
			cpuInactive++;

		//assign last in cpu to what just got procesed
		lastInCpu = cFront;

		//get first io queue process
		ioTemp = ioFront;

		//if something is in the io queue
		while(ioTemp != null)
		{
			//reduce io time of all process by 1
			ioTemp.setIoBurst(ioTemp.getIoBurst() - 1);
			ioTemp = ioTemp.getNext();
		}

		totalTime++;

		/*							 */
		/*							 */
		/*check for context switching*/
		/*							 */
		/*							 */

		/*io context switching*/

		if(!ioQueue.isEmpty())
		{
			int y = ioQueue.getSize();
			//while(ioFront != null)
			for(int x = 0; x < y; x++)
			{
				ioFront = ioQueue.getFront();

				//check if front process is done executing
				if(ioFront.getIoBurst() <= 0)
				{	
					if(whatFinishedIO == "")
						whatFinishedIO = Integer.toString(ioFront.getPid());
					else
						whatFinishedIO = whatFinishedIO + " & " + Integer.toString(ioFront.getPid());

					//if so put it in the temp queue
					tempQ.enqueue(ioQueue.dequeue());
				}
				else
				{	//cycle front to back to get to the next processes
					ioQueue.enqueue(ioQueue.dequeue());
				}
			}
		}
		
		while(!tempQ.isEmpty())
		{
			//set ioTemp to first item in tempQ
			//this is will point to the process with the lowers pid
			ioTemp = tempQ.getFront();

			//use cTemp so we dont have to declare another process
			//cTemp will scan through the queue to compare with ioTemp
			cTemp = ioTemp.getNext();
			while(cTemp != null)
			{
				//if cTemp's pid is less than ioTemp's make ioTemp point to cTemp
				if(cTemp.getPid() < ioTemp.getPid())
					ioTemp = cTemp;
				//get next item in queue
				cTemp = cTemp.getNext();
			}

			/*ioTemp now holds process with lowest pid*/

			//dequeue and enqueue until ioTemp to the front of queue
			while(tempQ.getFront().getPid() != ioTemp.getPid())
				tempQ.enqueue(tempQ.dequeue());

			//remove ioTemp from tempQ and put it into cpuQueue
			cpuQueue.enqueue(tempQ.dequeue());
		}

		/*CPU context switches*/

		//if there is something in "cpu"
		if(cFront != null)
		{
			//if front process is done executing
			if(cFront.getCpuBurst() == 0)
			{
				//save that this job finished executing
				whatFinished = "Job " + Integer.toString(cFront.getPid()) + " DONE\n";

				cFront.setTurnAroundTime(totalTime);
				completedQueue.insert(cpuQueue.dequeue(),'c');
				timePerBurst = 0;

				//procces is terminated
				cpuIsEmpty = true; 
				numContextSwitches++;
			}else
			//if its halfway completed and has a ioburst
			if(cFront.getCpuBurst() == cFront.getIoBurstStartTime())
			{
				//save that this job finished its cpu burst
				whatFinished = "Job " + Integer.toString(cFront.getPid()) + " finished CPU burst\n";

				cFront.setIoBurstStartTime(0);

				//remove front from the cpuqueue and add it to the ioqueue
				ioQueue.enqueue(cpuQueue.dequeue());
				timePerBurst = 0;

				//process must be contexted switch out/in
				cpuIsEmpty = true;
				numContextSwitches++;
			}else
			//if current process has "used up" its time quantum
			if(timePerBurst == timeQ && cpuQueue.getSize() > 1)
			{
				//save that this job used up its time quantum
				whatFinished = "Job " + Integer.toString(cFront.getPid()) + " has used up its time quantum\n";

				//if so move the front to the back
				cpuQueue.enqueue(cpuQueue.dequeue());
				timePerBurst = 0;

				//procces must be contexted switch out/in
				cpuIsEmpty = true; 
				numContextSwitches++;
			}
		}
	}

	public ProcessQueue getCpuQueue()
	{
		return cpuQueue;
	}

	public ProcessQueue getIoQueue()
	{
		return ioQueue;
	}

	public ProcessQueue getCompletedQueue()
	{
		return completedQueue;
	}

	public int getTotalTime()
	{
		return totalTime;
	}

	public String getCpuSequence()
	{
		return cpuSequence;
	}

	public float getThroughput()
	{
		return totalNumProcesses/(float)totalTime;
	}

	public float getCpuUtil()
	{
		return (totalTime - cpuInactive)/(float)totalTime;
	}

	public float getAvgWaitingTime()
	{
		return totalWaitTime/(float)totalNumProcesses;
	}

	public float getAvgTurnAroundTime()
	{
		return totalTurnaround/(float)totalNumProcesses;
	}

	public int getNumContextSwitches()
	{
		return numContextSwitches;
	}

	public void printCurrentState()
	{	
		Process temp = cpuQueue.getFront();
		//print current time
		System.out.println("\nt = " + totalTime);
		//print what finished
		System.out.print(whatFinished);

		//print state of cpu
		//if nothing is in cpu
		if(cpuIsEmpty)
		{
			//and there is nothing ready
			if(cpuQueue.isEmpty())
				System.out.println("CPU waiting");
			else
				System.out.println("CPU loading job " + Integer.toString(temp.getPid()) +
					": CPU burst(" + (temp.getCpuBurst() - temp.getIoBurstStartTime()) + ") IO burst(" + temp.getIoBurst() + ")");
		}
		else
		{
			System.out.println("Servicing RR job " + Integer.toString(temp.getPid()) +
					": CPU burst(" + (temp.getCpuBurst() - temp.getIoBurstStartTime()) + ") IO burst(" + temp.getIoBurst() + ")");
		}


		/*print ready queue*/

		System.out.print("Current state of ready queue: ");
		//if first process is in the cpu skip it
		if(!cpuIsEmpty)
			temp = temp.getNext();

		//print first ready
		if(temp != null)
		{
			System.out.print(Integer.toString(temp.getPid()));
			temp = temp.getNext();
		}

		//print rest ready
		while(temp != null)
		{
			System.out.print("-" + Integer.toString(temp.getPid()));
			temp = temp.getNext();
		}

		System.out.println();
		/*print IO queue*/

		//print what finished IO
		if(whatFinishedIO != "")
			System.out.println("Job(s) " + whatFinishedIO + " finished IO burst");

		//print current state of IO queue

		temp = ioQueue.getFront();

		System.out.print("Current state of IO queue: ");

		//print first (for formatting)
		if(temp != null)
		{
			System.out.print(Integer.toString(temp.getPid()));
			temp = temp.getNext();
		}

		//then print the rest (for formatting)
		while(temp != null)
		{
			System.out.print("-" + Integer.toString(temp.getPid()));
			temp = temp.getNext();
		}

		System.out.println();
	}

	public void printReport()
	{
		System.out.print("\nFinal report for priority algorithm.\n\nThroughput: ");

		System.out.println(this.getThroughput() + "\n\nProcess ID\t Wait Time");
		for(int x = 0; x < totalNumProcesses; x++)
		{
			//print pid and wait time
			System.out.println("    " + completedQueue.getFront().getPid() + "\t\t    " + completedQueue.getFront().getWaitTime());
			//add all wait times while we are at it
			totalWaitTime += completedQueue.getFront().getWaitTime();
			completedQueue.enqueue(completedQueue.dequeue());
		}
		System.out.println("Average waiting time = " + this.getAvgWaitingTime());

		System.out.println("\nCPU utilization: " + (int)(this.getCpuUtil()*100) + "%");

		System.out.println("\nSequence of process in CPU: " + cpuSequence);


		System.out.println("\nProcess ID\t Turnaround Time");
		for(int x = 0; x < totalNumProcesses; x++)
		{
			//print pid and wait time
			System.out.println("    " + completedQueue.getFront().getPid() + "\t\t\t" + completedQueue.getFront().getTurnAroundTime());
			//add all wait times while we are at it
			totalTurnaround += completedQueue.getFront().getTurnAroundTime();
			completedQueue.enqueue(completedQueue.dequeue());
		}
		System.out.println("Average turnaround time = " + this.getAvgTurnAroundTime());

		System.out.println("\nNumber of context switches " + this.getNumContextSwitches());

	}
}
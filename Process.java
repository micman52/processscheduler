/*MacKinley Trudeau*/


public class Process
{
	//ALE ADDED WAIT TIME and Waittime. 

	private int finishTime;

	//time the process spent waiting in reader queue
	private int waitTime;
	//time that the process terminated
	private int turnAroundTime;
	// Is when the IO cycle starts. = cpuBurst / 2;
	// When cpuBurst = ioBurstStartTime, process moves from normal queue to ioQueue.
	private int ioBurstStartTime;
	//totalTime is used in wait time calculations.
	private int totalTime;
	//processes id
	private int pid;
	//time units until CPU burst is completed
	private int cpuBurst;
	//time unitsubtuk I/O burst is completed
	private int ioBurst;
	//prioity of the process (lower number means higher prioity?)
	private int priority;
	//next process to arrive
	private Process next;

	//no arg constructor
	public Process()
	{
		pid = 0;
		cpuBurst = 0;
		ioBurst = 0;
		priority = 0;
	
	}
	//full arg constructor
	public Process(int id, int cB, int iB, int p)
	{
		pid = id;
		cpuBurst = cB;
		ioBurst = iB;
		priority = p;
		ioBurstStartTime = cB/2;
		totalTime = cpuBurst + ioBurst;
	}

	
	public int getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}

	public int getTurnAroundTime() {
		return turnAroundTime;
	}

	public void setTurnAroundTime(int turnAroundTime) {
		this.turnAroundTime = turnAroundTime;
	}

	public int getIoBurstStartTime() {
		return ioBurstStartTime;
	}

	public void setIoBurstStartTime(int ioBurstStartTime) {
		this.ioBurstStartTime = ioBurstStartTime;
	}

	public int getPid() {
		return pid;
	}
	
	public void setPid(int p) {
		this.pid = p;
	}
	
	//set cpuBurst
	public void setCpuBurst(int cB)
	{
		cpuBurst = cB;
	}

	//get cpuBurst
	public int getCpuBurst()
	{
		return cpuBurst;
	}

	//set ioBurst
	public void setIoBurst(int iB)
	{
		ioBurst = iB;
	}

	//get ioBurst
	public int getIoBurst()
	{
		return ioBurst;
	}

	//set prioity
	public void setPriority(int p)
	{
		priority = p;
	}

	//get prioity
	public int getPriority()
	{
		return priority;
	}

	//set next
	public void setNext(Process n)
	{
		next = n;
	}

	//get next
	public Process getNext()
	{
		return next;
	}
}